class QRinput {

    var $items;
    
    var $version;
    var $level;
    
    //----------------------------------------------------------------------
    function QRinput($version = 0, $level = QR_ECLEVEL_L)
    {
        if ($version < 0 || $version > QRSPEC_VERSION_MAX || $level > QR_ECLEVEL_H) {
            throw ("MyException", "Invalid version no");
            return NULL;
        }
        
        this.$version = $version;
        this.$level = $level;
    }
    
    //----------------------------------------------------------------------
    function getVersion()
    {
        return this.$version;
    }
    
    //----------------------------------------------------------------------
    function setVersion($version)
    {
        if($version < 0 || $version > QRSPEC_VERSION_MAX) {
            throw ("MyException", "Invalid version no");
            return -1;
        }

        this.$version = $version;

        return 0;
    }
    
    //----------------------------------------------------------------------
    function getErrorCorrectionLevel()
    {
        return this.$level;
    }

    //----------------------------------------------------------------------
    function setErrorCorrectionLevel($level)
    {
        if($level > QR_ECLEVEL_H) {
            throw ("MyException", "Invalid ECLEVEL");
            return -1;
        }

        this.$level = $level;

        return 0;
    }
    
    //----------------------------------------------------------------------
    function appendEntry(QRinputItem $entry)
    {
        if(this.$items == null) this.$items = [];
        this.$items.add($entry);
    }
    
    //----------------------------------------------------------------------
    function append($mode, $size, $data)
    {
        try {
            var $entry = new QRinputItem($mode, $size, $data);
            if(this.$items == null) this.$items = [];
            this.$items.add($entry);

            return 0;
        } catch ($e) {
            print "exception:=>append()" + $e;
            return -1;
        }
    }
    
    //----------------------------------------------------------------------
    
    function insertStructuredAppendHeader($size, $index, $parity)
    {
        if( $size > MAX_STRUCTURED_SYMBOLS ) {
            throw ("MyException", "insertStructuredAppendHeader wrong size");
        }
        
        if( $index <= 0 || $index > MAX_STRUCTURED_SYMBOLS ) {
            throw ("MyException", "insertStructuredAppendHeader wrong index");
        }

        var $buf = new Array($size, $index, $parity);
        
        try {
            $entry = new QRinputItem(QR_MODE_STRUCTURE, 3, buf);
            php::array_unshift(this.$items, $entry);
            return 0;
        } catch ($e) {
            print "exception:=>insertStructuredAppendHeader()" + $e;
            return -1;
        }
    }

    //----------------------------------------------------------------------
    function calcParity()
    {
        var $parity = 0;
        
        // foreach(this.$items as $item) {
        //     if($item.mode != QR_MODE_STRUCTURE) {
        //         for($i=$item.size-1; $i>=0; $i--) {
        //             $parity ^= $item.data[$i];
        //         }
        //     }
        // }
        foreach (k,$item : this.$items )
        {
            if($item.mode != QR_MODE_STRUCTURE) {
                for(var $i=$item.size-1; $i>=0; $i--) {
                    $parity ^= $item.data[$i];
                }
            }
        }

        return $parity;
    }
    
    //----------------------------------------------------------------------
    function checkModeNum($size, $data)
    {
        for(var $i=0; $i<$size; $i++) {
            if((php::ord($data[$i]) < php::ord("0")) || (php::ord($data[$i]) > php::ord("9"))){
                return false;
            }
        }

        return true;
    }

    //----------------------------------------------------------------------
    function estimateBitsModeNum($size)
    {
        // var $w = INT$size / 3;
        var $w = INT($size) / 3;
        var $bits = $w * 10;
        
        switch($size - $w * 3) {
            case 1:{
                $bits += 4;
                break;
            }
            case 2:{
                $bits += 7;
                break;
            }
            default:{
                break;
            }
        }

        return $bits;
    }
    
    //----------------------------------------------------------------------
    var $anTable = new Array(
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43,
            0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 44, -1, -1, -1, -1, -1,
        -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
        25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    );
    
    //----------------------------------------------------------------------
    function lookAnTable($c)
    {
        if($c > 127) return  -1;
        else return this.$anTable[$c];
    }
    
    //----------------------------------------------------------------------
    function checkModeAn($size, $data)
    {
        for(var $i=0; $i<$size; $i++) {
            if (this.lookAnTable(php::ord($data[$i])) == -1) {
                return false;
            }
        }

        return true;
    }
    
    //----------------------------------------------------------------------
    function estimateBitsModeAn($size)
    {
        var $w = INT($size / 2);
        var $bits = $w * 11;
        
        if($size & 1) {
            $bits += 6;
        }

        return $bits;
    }

    //----------------------------------------------------------------------
    function estimateBitsMode8($size)
    {
        return $size * 8;
    }
    
    //----------------------------------------------------------------------
    function estimateBitsModeKanji($size)
    {
        return INT(($size / 2) * 13);
    }
    
    //----------------------------------------------------------------------
    function checkModeKanji($size, $data)
    {
        if($size & 1)
            return false;

        for(var $i=0; $i<$size; $i+=2) {
            $val = (php::ord($data[$i]) << 8) | php::ord($data[$i+1]);
            if( $val < 0x8140 
            || ($val > 0x9ffc && $val < 0xe040) 
            || $val > 0xebbf) {
                return false;
            }
        }

        return true;
    }

    // /***********************************************************************
    //  * Validation
    //  **********************************************************************/

    function check($mode, $size, $data)
    {
        if($size <= 0) 
            return false;

        switch($mode) {
            case 0:/*QR_MODE_NUM*/       {return this.checkModeNum($size, $data);   break;}
            case 1:/*QR_MODE_AN*/        {return this.checkModeAn($size, $data);    break;}
            case 3:/*QR_MODE_KANJI*/     {return this.checkModeKanji($size, $data); break;}
            case 2:/*QR_MODE_8*/         {return true; break;}
            case 4:/*QR_MODE_STRUCTURE*/ {return true; break;}
            
            default:{
                break;
            }
        }

        return false;
    }
    
    
    //----------------------------------------------------------------------
    function estimateBitStreamSize($version)
    {
        var $bits = 0;

        // foreach(this.$items as $item) {
        //     $bits += $item.estimateBitStreamSizeOfEntry($version);
        // }
        foreach (k,$item : this.$items)
        {
            $bits += $item.estimateBitStreamSizeOfEntry($version);
        }

        return $bits;
    }
    
    //----------------------------------------------------------------------
    function estimateVersion()
    {
        var $version = 0;
        var $prev = 0;
        do {
            $prev = $version;
            var $bits = this.estimateBitStreamSize($prev);
            $version = QRspec::getMinimumVersion(INT(($bits + 7) / 8), this.$level);
            if ($version < 0) {
                return -1;
            }
        } while ($version > $prev);

        return $version;
    }
    
    //----------------------------------------------------------------------
    function lengthOfCode($mode, $version, $bits)
    {
        var $chunks ;
        var $remain ;
        var $size ;
        var $payload = $bits - 4 - QRspec::lengthIndicator($mode, $version);
        switch($mode) {
            case 0:{ /*QR_MODE_NUM*/
                $chunks = INT($payload / 10);
                $remain = $payload - $chunks * 10;
                $size = $chunks * 3;
                if($remain >= 7) {
                    $size += 2;
                } else if($remain >= 4) {
                    $size += 1;
                }
                break;
            }
            case 1:{/*QR_MODE_AN*/
                $chunks = INT($payload / 11);
                $remain = $payload - $chunks * 11;
                $size = $chunks * 2;
                if($remain >= 6) 
                    $size++;
                break;
            }
            case 2:{/*QR_MODE_8*/
                $size = INT($payload / 8);
                break;
            }
            case 3:{/*QR_MODE_KANJI*/
                $size = INT(($payload / 13) * 2);
                break;
            }
            case 4:{ /*QR_MODE_STRUCTURE*/
                $size = INT($payload / 8);
                break;
            }
            default:{
                $size = 0;
                break;
            }
        }
        
        var $maxsize = QRspec::maximumWords($mode, $version);
        if($size < 0) $size = 0;
        if($size > $maxsize) $size = $maxsize;

        return $size;
    }
    
    //----------------------------------------------------------------------
    function createBitStream()
    {
        var $total = 0;

        // foreach(this.$items as $item) {
        //     $bits = $item.encodeBitStream(this.$version);
            
        //     if($bits < 0) 
        //         return -1;
                
        //     $total += $bits;
        // }
        foreach (k,$item : this.$items)
        {
            var $bits = $item.encodeBitStream(this.$version);
            
            if($bits < 0) 
                return -1;
                
            $total += $bits;
        }
        
        return $total;
    }
    
    //----------------------------------------------------------------------
    function convertData()
    {
        var $ver = this.estimateVersion();
        if($ver > this.getVersion()) {
            this.setVersion($ver);
        }

        for(;;) {
            var $bits = this.createBitStream();
            if($bits < 0) 
                return -1;
                
            $ver = QRspec::getMinimumVersion(INT(($bits + 7) / 8), this.$level);
            if($ver < 0) {
                throw ("MyException", "WRONG VERSION");
                return -1;
            } else if($ver > this.getVersion()) {
                this.setVersion($ver);
            } else {
                break;
            }
        }

        return 0;
    }
    
    //----------------------------------------------------------------------
    // function appendPaddingBit(&$bstream)
    function appendPaddingBit($bstream)
    {
        var $bits = $bstream.size();
        var $maxwords = QRspec::getDataLength(this.$version, this.$level);
        var $maxbits = $maxwords * 8;

        if ($maxbits == $bits) {
            return 0;
        }

        if ($maxbits - $bits < 5) {
            return $bstream.appendNum($maxbits - $bits, 0);
        }

        $bits += 4;
        var $words = INT(($bits + 7) / 8);

        var $padding = new QRbitstream();
        var $ret = $padding.appendNum($words * 8 - $bits + 4, 0);
        
        if($ret < 0) 
            return $ret;

        var $padlen = $maxwords - $words;
        
        if($padlen > 0) {
            
            var $padbuf = new Array();
            for(var $i=0; $i<$padlen; $i++) {
                $padbuf.add( php::chose($i&1, 0x11, 0xec) );
            }
            
            $ret = $padding.appendBytes($padlen, $padbuf);
            if($ret < 0)
                return $ret;
            
        }

        $ret = $bstream.append($padding);
        
        return $ret;
    }

    //----------------------------------------------------------------------
    function mergeBitStream()
    {
        if(this.convertData() < 0) {
            return null;
        }

        var $bstream = new QRbitstream();
        
        // foreach(this.$items as $item) {
        //     $ret = $bstream.append($item.bstream);
        //     if($ret < 0) {
        //         return null;
        //     }
        // }
        foreach (k,$item : this.$items)
        {
            var $ret = $bstream.append($item.$bstream);
            if($ret < 0) {
                return null;
            }
        }

        return $bstream;
    }

    //----------------------------------------------------------------------
    function getBitStream()
    {

        var $bstream = this.mergeBitStream();
        
        if($bstream == null) {
            return null;
        }
        
        var $ret = this.appendPaddingBit($bstream);
        if($ret < 0) {
            return null;
        }

        return $bstream;
    }
    
    //----------------------------------------------------------------------
    function getByteStream()
    {
        var $bstream = this.getBitStream();
        if($bstream == null) {
            return null;
        }
        
        return $bstream.toByte();
    }
}