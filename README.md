# cbrother-for-qrcode

#### 介绍
cbrother-for-qrcode 是基于phpqrcode，使用CBrother语言重构的。 

#### 安装教程

1. 在cbrother安装根目录lib文件夹下，克隆我使用CBrother封装的php函数项目，方便全局调用这些函数，参考：https://gitee.com/wenzhihzhon/cbrother-for-php-function 比如，D:\cbrother_v2.4.5_win_amd64\lib\cbrotherForPhpFunction\php.cb
2. 克隆项目，克隆当前项目cbrother-for-qrcode下来： 
```
    git clone https://gitee.com/wenzhihzhon/cbrother-for-qrcode cbrotherForQrcode
```

#### 使用说明
```
import lib/cbrotherForPhpFunction/php
import ./QRcode.cb

function main()
{
    var qRimage = new QRcode();
    var $res = qRimage.png("http://www.cbrother.net/main.html", "11111.png");
    php::dump($res);
}
```

#### 致谢
[phpqrcode](https://phpqrcode.sourceforge.net/)  [CBrother官网](http://www.cbrother.net/main.html)  
